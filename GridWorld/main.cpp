//
//  main.cpp
//  GridWorld
//
//  Created by Jayden O'Brien on 7/8/17.
//  Copyright © 2017 Jayden O'Brien. All rights reserved.
//

#include <iostream>
#include <ctype.h>
#include <string>

#define log(x) std::cout << x << std::endl;

char mapData[8][8] = {'#','#','#','#','#','#','#','#',
                  '#','G',' ','D','#','D',' ','#',
                  '#',' ',' ',' ','#',' ',' ','#',
                  '#','#','#',' ','#',' ','D','#',
                  '#',' ',' ',' ','#',' ',' ','#',
                  '#',' ','#','#','#','#',' ','#',
                  '#',' ',' ',' ',' ',' ',' ','#',
                  '#','#','S','#','#','#','#','#'};

int playerX, playerY;

enum gameState {alive, dead, quit, winner};

gameState state = alive;


void drawMap()
{
    for (int i =0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            if(playerX == j && playerY == i)
            {
                std::cout << 'O';
            }
            else
            {
                std::cout << mapData[i][j];
            }
            
        }
        std::cout << std::endl;
    }
}

void playerCommand(char playerInput)
{
    switch(playerInput)
    {
        case 'n':
            if (mapData[playerY-1][playerX] != '#' && playerY > 0)
                playerY--;
            break;
        case 's':
            if (mapData[playerY+1][playerX] != '#' && playerY < 7)
                playerY++;
            break;
        case 'e':
            if (mapData[playerY][playerX+1] != '#' && playerX < 7)
                playerX++;
            break;
        case 'w':
            if (mapData[playerY][playerX-1] != '#' && playerX > 0)
                playerX--;
            break;
        case 'q':
            state = quit;
            break;
        default:
            std::cout << "Invalid Command" << std::endl;
            break;
    }
    if (mapData[playerY][playerX] == 'G')
    {
        state = winner;
    }
    else if (mapData[playerY][playerX] == 'D')
    {
        state = dead;
    }
}

void drawGame()
{
    std::string output;
    drawMap();
    if (state == alive)
    {
        output.append("You can move ");
        if (mapData[playerY-1][playerX] != '#' && playerY > 0)
        {
            output.append("N,");
        }
        if (mapData[playerY+1][playerX] != '#' && playerY < 7)
        {
            output.append("S,");
        }
        if (mapData[playerY][playerX+1] != '#' && playerX < 7)
        {
            output.append("E,");
        }
        if (mapData[playerY][playerX-1] != '#' && playerX > 0 )
        {
            output.append("W");
        }
        if (output.back() == ',')
            output.pop_back();
        output.append(":>");
    }
    if (state == winner)
    {
        output.append("You found the gold! :)");
    }
    if (state == dead)
    {
        output.append("You have died! :(");
    }
    std::cout << output;
}

void getSpawn()
{
    for (int i =0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            if (mapData[i][j] == 'S')
            {
                playerX = j;
                playerY = i;
                return;
            }
        }
    }
}

//unintended feature, entering multiple commands does them all
//entering 'nwnneennwwn' wins the game
int main(int argc, const char * argv[]) {
    char playerInput;
    getSpawn();
    drawGame();
    do
    {
        std::cin >> playerInput;
        playerInput = tolower(playerInput);
        playerCommand(playerInput);
        drawGame();
        
        
    }while(state == alive);
    
    return 0;
}
