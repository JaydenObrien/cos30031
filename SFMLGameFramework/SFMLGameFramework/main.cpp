#include <iostream>
#include <string>
#include <stdlib.h>
#include <SFML\Graphics.hpp>
#include <SFML\Window\Keyboard.hpp>
#include <SFML\Audio.hpp>
#include <SFML\Main.hpp>
#include <SFML\System.hpp>
#include "KeyBoardTest.h"
#include "SoundTest.h"
#include "TextureTest.h"
#include "Circle.h"
#include <chrono>
#include "CircleColTest.h"
#include "Rectangle.h"
#include "RectColTest.h"
#include "sprite.h"
#include "PixelPerfectTest.h"
#define log(x) std::cout << x << std::endl

int main()
{
	
	sf::RenderWindow window(sf::VideoMode(800, 600), "Window Memes");



	sf::Font font;
	font.loadFromFile("fonts/arial.ttf");
	sf::Text fps("FPS", font, 50);
	fps.setFillColor(sf::Color::Black);

	RectColTest test(200, sf::Vector2f(20, 20));
	test.init();
	auto start = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> elapsed;
	while (window.isOpen())
	{
		std::chrono::duration<double> elapsed = std::chrono::high_resolution_clock::now() - start;
		start = std::chrono::high_resolution_clock::now();

		sf::Event e;
		window.pollEvent(e);

		if (e.type == sf::Event::Closed)
			window.close();
		test.update(elapsed.count());
		
		test.draw(&window);
		window.draw(fps);
		window.display();
		window.clear(sf::Color::White);
		fps.setString(std::to_string((int)(1/elapsed.count())));
	}
	
	return 0;
}