#define _CRTDBG_MAP_ALLOC  
#include <string>
#include <iostream>
#include <stdexcept>
#include <memory>

#define debug 0;

using namespace std;


template <class DataType>
class DoubleLinkedNode {
public:
	typedef DoubleLinkedNode<DataType> Node;

	
	DoubleLinkedNode(const DataType initvalue) : value(initvalue) {
		//cout << "Creating Node..." << endl;
	}

	
	~DoubleLinkedNode() {
		//cout << "Destroying Node... " << endl;
	}

	
	void insertNodeAfter(Node* newnode) {
		if (nextnode) {
			newnode->insertNodeAfter(nextnode);
		}
		setNext(newnode);
		newnode->setPrevious(this);
	};

	
	void insertNodeBefore(Node* newnode) {
		if (previousnode) {
			newnode->insertNodeBefore(previousnode); //change this
		}
		setPrevious(newnode);
		newnode->setNext(this);
	};

	
	void dropNode() {
		if (previousnode) {
			previousnode->setNext(nextnode);
		}
		if (nextnode) {
			nextnode->setPrevious(previousnode);
		}
		delete this;
	};

	
	Node* getNext() const {
		return nextnode;
	};

	
	Node* getPrevious() const {
		return previousnode;
	};

	
	void setNext(Node* N) {
		nextnode = N;
	}

	void setPrevious(Node* N) {
		previousnode = N;
	}

	
	const DataType& getValue() const {
		return value;
	};

private:
	//The data contained in this node
	DataType value;
	//The next Node in the list
	Node* nextnode;
	//The previous Node in the list
	Node* previousnode;
};


template <class T>
class DoubleLinkedList {
private:
	typedef DoubleLinkedNode<T> Node;

	//The first node in the list
	Node* first;
	//The last node in the list
	Node* last;
	//The length of the list
	int _length;
public:

	DoubleLinkedList() : first(0), last(0), _length(0) {};


	~DoubleLinkedList() {
		while (first->getNext() != (Node*)0) {
			first->getNext()->dropNode();
		}
		first->dropNode(); //ay
	};

	void append(const T &newelement) {
		Node *N = new Node(newelement);

		if (first == (Node*)0) {
			first = N;
			last = N;
			_length = 1;
		}
		else {
			last->insertNodeAfter(N);
			last = N;
			_length++;
		}
	};


	void prepend(const T &newelement) {
		Node *N = new Node(newelement);

		if (first == (Node*)0) {
			first = N;
			last = N;
			_length = 1;
		}
		else {
			first->insertNodeBefore(N);
			first = N;
			_length++;
		}
	};

	bool drop(const T &element) {
		if (first->getValue() == element) {
			dropFirst();
			return true;
		}
		else if (last->getValue() == element) {
			dropLast();
			return true;
		}

		Node *N = first;
		while (N != last) {
			if (N->getValue() == element) {
				N->dropNode();
				return true;
			}
			else 
			{
				N = N->getNext();//added this thing so it itteratiates
			}
		}
		return false;
	};


	void dropFirst() {
		first = first->getNext();
		first->getPrevious()->dropNode();
	};


	void dropLast() {
		last = last->getPrevious();
		last->getNext()->dropNode();
	};

	void print(void) {
		//Assign N to first
		Node *N = first;
		while (N != last) {
			cout << N->getValue() << endl;
			N = N->getNext();
		}
		cout << N->getValue() << endl; //it prints the last value
		cout << "-----------------------------------------------" << endl;
	}
};

void memleaktest()
{
	string s1("One");
	string s2("Two");
	string s3("Three");
	string s4("Four");
	string s5("Five");
	string s6("Six");

	//made a DLL and assigned the pointer L to it
	DoubleLinkedList<string>* L = new DoubleLinkedList<string>();

	//Add some numbers to the list
	L->append(s3);
	L->append(s4);
	L->append(s5);

	L->prepend(s1);
	L->prepend(s2);

	L->drop(s1);
	L->drop(s2);


	L->prepend(s2);
	L->prepend(s1);


	L->append(s6); //Change this to append
	//Done!
}

int main(int argc, char* argv[]) {
	
#if debug
	for (;;)
	{
		memleaktest();
	}

#else
	
	string s1("One");
	string s2("Two");
	string s3("Three");
	string s4("Four");
	string s5("Five");
	string s6("Six");

	//made a DLL and assigned the pointer L to it
	DoubleLinkedList<string>* L = new DoubleLinkedList<string>();

	//Add some numbers to the list
	L->append(s3);
	L->append(s4);
	L->append(s5);
	L->print();		//Looks good, but we forgot One and Two
					//Lets add them
	L->prepend(s1);
	L->prepend(s2);
	L->print();		//Oh, no - they're on backwards
					//Remove them
	L->drop(s1);
	L->drop(s2);
	L->print();		//Yep, they're gone
					//Add them again, this time in the right order.
	L->prepend(s2);
	L->prepend(s1);
	L->print();		//All good
					//add the last number
	L->append(s6); //Change this to append
	L->print();		//Done!

#endif
	_CrtDumpMemoryLeaks(); //this produces report to the debug console
	std::cin.get();
	return 0;
}

