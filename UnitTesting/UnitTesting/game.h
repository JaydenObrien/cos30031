#pragma once


class Game {
public:
	char mapData[8][8] = { '#','#','#','#','#','#','#','#',
		'#','G',' ','D','#','D',' ','#',
		'#',' ',' ',' ','#',' ',' ','#',
		'#','#','#',' ','#',' ','D','#',
		'#',' ',' ',' ','#',' ',' ','#',
		'#',' ','#','#','#','#',' ','#',
		'#',' ',' ',' ',' ',' ',' ','#',
		'#','#','S','#','#','#','#','#' };

	int playerX, playerY;

	enum gameState { alive, dead, quit, winner };

	gameState state = alive;
	Game() {};

	void drawMap();

	void playerCommand(char playerInput);

	void drawGame();

	void getSpawn();
};