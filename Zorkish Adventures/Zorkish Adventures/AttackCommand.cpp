#include "CommandInterface.h"
#include "Inventory.h"

void AttackCommand::command(player& p, const std::vector<std::string>& tokens)
{
	if (tokens.size() == 4 && tokens[2] == "with")
	{
		if (p.getLocation()->getInventory()->getItem(tokens[1]))
		{
			if (p.getInventory().getItem(tokens[3]))
			{
				if (p.getLocation()->getInventory()->getItem(tokens[1])->hasAttribute<HealthPoints>())
				{
					if (p.getInventory().getItem(tokens[3])->hasAttribute<damage>())
					{
						p.getLocation()->getInventory()->getItem(tokens[1])->getAttribute<HealthPoints>()->removeHP(p.getInventory().getItem(tokens[3])->getAttribute<damage>()->getValue());
						cout << "You attack " + tokens[1] + " with a " + tokens[3] + ": dealing " + std::to_string(p.getInventory().getItem(tokens[3])->getAttribute<damage>()->getValue()) + " damage." << endl;
						if (p.getLocation()->getInventory()->getItem(tokens[1])->getAttribute<HealthPoints>()->isDead())
						{
							cout << "You killed " + tokens[1] + ". It spews its inventory all over the ground." << endl;
							p.getLocation()->getInventory()->addItems(p.getLocation()->getInventory()->getItem(tokens[1])->getAttribute<Inventory>()->getAllItems());
							p.getLocation()->getInventory()->removeItem(tokens[1]);
						}
					}
					else
					{
						cout << "You can't attack with that" << endl;
					}
				}
				else
				{
					cout << "You can't attack that" << endl;
				}
			}
			else
			{
				cout << "You don't have that item in your inventory to attack with" << endl;
			}
		}
		else
		{
			cout << "There is no item with that name that you can attack" << endl;
		}
	}
	else
	{
		cout << "Invalid attack command" << endl;
	}
}