#include "Attribute.h"

Attribute::Attribute()
{
}

Attribute::~Attribute()
{
	delete inv;
	delete hp;
	delete dmg;
	delete mov;
}


void Attribute::addInventoryAttribute()
{ 
	delete inv; 
	inv = new Inventory(); 
}

Inventory* Attribute::getInventory() 
{ 
	return inv; 
}

void Attribute::addHpAttribute(int amount)
{ 
	delete hp; 
	hp = new HealthPoints(amount);
}

HealthPoints* Attribute::getHp() 
{ 
	return hp; 
}

void Attribute::addDamageAttribute(int value)
{
	delete dmg;
	dmg = new damage(value);
}

damage* Attribute::getDamage()
{
	return dmg;
}

void Attribute::addMoveableAttribute(bool value)
{
	delete mov;
	mov = new Moveable(value);
}

Moveable* Attribute::getMoveable()
{
	return mov;
}
