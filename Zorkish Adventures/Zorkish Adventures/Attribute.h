#pragma once
#include <string>
#include "Item.h"

class Attribute
{
public:
	virtual ~Attribute() {};
	
	virtual std::string print() = 0;
};


class damage : public Attribute
{
private:
	int value;
public:
	damage(int value) { this->value = value; }
	int getValue() { return value; }
	void setValue(int value) { this->value = value; }
	std::string print() { return "Dmg: " + std::to_string(value); }
};

class locked : public Attribute
{
private:
	bool value;
	std::string keyID;
public:
	locked(int value, std::string keyID)
	{
		this->value = value;
		this->keyID = keyID;
	}


	bool isLocked() 
	{
		if (this)
			return value;

		return false;
	}

	bool unlock(std::string keyID)
	{
		if (this->keyID == "")
		{
			value = false;
			return true;
		}
		else if (this->keyID == keyID)
		{
			value = false;
			return true;
		}
		return false;
	}

	bool unlock()
	{
		if (this->keyID == "")
		{
			value = false;
			return true;
		}
		return false;
	}

	void lock() { value = true; }

	std::string print() { return value ? "Locked" : "unlocked"; }
};

class HealthPoints : public Attribute
{
private:
	int hp;
public:
	HealthPoints(int hp) { this->hp = hp; }

	void addHP(int amount) { hp = hp + amount; }
	void removeHP(int amount) { hp = hp - amount; }
	bool isDead() { return hp <= 0; }
	int getHp() { return hp; }
	std::string print() { return "HP: " + std::to_string(hp); }
};

class Immovable : public Attribute
{
public:

	std::string print() { return "Immovable"; }
};

