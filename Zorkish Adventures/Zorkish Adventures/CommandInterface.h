#pragma once
#include <string>
#include <vector>
#include "player.h"
#include "pasta.h"
#include "Attribute.h"
#include "Inventory.h"


class CommandInterface 
{
private:
	std::string word;
public:
	CommandInterface(std::string word) { this->word = word; }
	virtual ~CommandInterface() {};
	virtual void command(player& p, const std::vector<std::string>& tokens) = 0;
	std::string getWord() { return word; }
};

class LookCommand: public CommandInterface 
{
public:
	LookCommand() : CommandInterface("look") {};
	virtual void command(player& p, const std::vector<std::string>& tokens);
};

class TakeCommand: public CommandInterface
{
public:
	TakeCommand() : CommandInterface("take") {};
	virtual void command(player& p, const std::vector<std::string>& tokens);
};

class OpenCommand : public CommandInterface
{
public:
	OpenCommand() : CommandInterface("open") {};
	virtual void command(player& p, const std::vector<std::string>& tokens);
};

class GoCommand : public CommandInterface
{
public:
	GoCommand() : CommandInterface("go") {};
	virtual void command(player& p, const std::vector<std::string>& tokens);
};

class AttackCommand : public CommandInterface
{
public:
	AttackCommand() : CommandInterface("attack") {};
	virtual void command(player& p, const std::vector<std::string>& tokens);
};

class PutCommand : public CommandInterface
{
public:
	PutCommand() : CommandInterface("put") {};
	virtual void command(player& p, const std::vector<std::string>& tokens);
};

