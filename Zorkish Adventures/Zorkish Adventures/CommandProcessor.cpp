#include "CommandProcessor.h"
#include <locale>

CommandProcessor::CommandProcessor()
{
	addCommand(new LookCommand());
	addCommand(new OpenCommand());
	addCommand(new TakeCommand());
	addCommand(new GoCommand());
	addCommand(new AttackCommand());
	addCommand(new PutCommand());
}

CommandProcessor::~CommandProcessor()
{
}

void CommandProcessor::addCommand(CommandInterface *c)
{
	alias.insert_or_assign(c->getWord(), c->getWord());
	commands.insert_or_assign(c->getWord(), c);
}

void CommandProcessor::command(player & p, std::vector<std::string>& tokens)
{
	for (auto& a : tokens)
	{
		std::locale loc;
		for (auto& b : a)
		{
			std::tolower(b, loc);
		}
	}

	if (tokens[0] == "alias")
	{
		if (tokens.size() == 3)
		{
			if (commands.count(tokens[1]))
			{
				addAlias(tokens[2], tokens[1]);
			}
			else
			{
				cout << "No command that can be made alias of" << endl;
			}
		}
		else 
		{
			cout << "Invalid alias command" << endl;
		}
	}
	else
	{
		if (commands[alias[tokens[0]]])
			commands[alias[tokens[0]]]->command(p, tokens);
		else
			cout << "No command that exist" << endl;
	}
	
}

void CommandProcessor::addAlias(std::string a, std::string b)
{
	alias.insert_or_assign(a, b);
}

bool CommandProcessor::hasCommand(std::vector<std::string>& tokens)
{
	if (alias.count(tokens[0]) || tokens[0] == "alias")
	{
		return true;
	}
	return false;

}
