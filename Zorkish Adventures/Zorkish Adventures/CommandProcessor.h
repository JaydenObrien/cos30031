#pragma once
#include <map>
#include "CommandInterface.h"

class CommandProcessor
{
private:
	std::map<string, string> alias;
	std::map<std::string, CommandInterface*> commands;
public:
	CommandProcessor();
	~CommandProcessor();

	void addCommand(CommandInterface*);
	void command(player& p, std::vector<std::string>& tokens);
	void addAlias(std::string, std::string);
	bool hasCommand(std::vector<std::string>& tokens);
};

