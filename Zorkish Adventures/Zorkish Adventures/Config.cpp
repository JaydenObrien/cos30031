#include "Config.h"
#include <fstream>
#include "pasta.h"

Config::Config()
{
}


Config::~Config()
{
}

void Config::loadConfig(std::string filepath)
{
	std::fstream file;
	file.open(filepath, std::ios::in, std::ios::binary);

	if (file.is_open())
	{
		std::string line;
		std::vector<std::string> temp;
		while (getline(file, line))
		{
			pasta::pasta(line, '=', temp);
			data.insert_or_assign(temp.at(0), temp.at(1));
			temp.clear();
		}

	}
	file.close();
}



