#pragma once
#include <map>
#include <string>
#include <type_traits>



class Config
{
private:
	std::map<std::string, std::string> data;

public:
	Config();
	~Config();

	void loadConfig(std::string filepath);

	
	std::string get(std::string d)
	{
		return data.at(d);
	}

	template<typename T> 
	T get(std::string d)
	{
		if (std::is_same<T, int>())
		{
			return std::stoi(data.at(d));
		}
		else if (std::is_same<T, float>())
		{
			return std::stof(data.at(d));
		}
		return static_cast<T>(0);
	}

};
