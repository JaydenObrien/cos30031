#include "CommandInterface.h"

void GoCommand::command(player& p, const std::vector<std::string>& tokens)
{
	if (tokens.size() == 2)
	{
		if (p.getLocation()->getRoute(tokens[1]))
		{
			p.setLocation(p.getLocation()->getRoute(tokens[1]));
			cout << "Heading to a " + tokens[1] << endl;
		}
		else
		{
			cout << "No location matching that name" << endl;
		}
	} 
	else if (tokens.size() == 3 && tokens[1] == "to")
	{
		if (p.getLocation()->getRoute(tokens[2]))
		{
			p.setLocation(p.getLocation()->getRoute(tokens[2]));
			cout << "Heading to a " + tokens[2] << endl;
		}
		else
		{
			cout << "No location matching that name" << endl;
		}
	}
	else
	{
		cout << "Invalid go command" << endl;
	}
}