#include "Inventory.h"

void Inventory::addItem(Item *item)
{
	inv.insert_or_assign(item->getName(), item);
}

void Inventory::removeItem(std::string k)
{
	inv.erase(k);
}

Item* Inventory::getItem(std::string k)
{
	return inv[k];
}

std::vector<Item*> Inventory::getAllItems()
{
	std::vector<Item*> temp;
	for (auto a : inv)
	{
		temp.push_back(a.second);
	}
	return temp;
}

void Inventory::addItems(std::vector<Item*> items)
{
	for (auto i : items)
	{
		addItem(i);
	}
}

std::string Inventory::print()
{
	std::string output = "";
	for (const auto& i: inv)
	{
		output.append(i.first + "\n");
	}
	if (output == "")
		return "Nothing";
	output.pop_back();
	return output;
}
