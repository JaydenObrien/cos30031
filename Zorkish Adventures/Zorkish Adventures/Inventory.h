#pragma once
#include <string>
#include <map>
#include <vector>
#include "Attribute.h"

class Item;
class Inventory: public Attribute
{
private:
	std::map<std::string, Item*> inv;
public:
	void addItem(Item*);
	void removeItem(std::string);
	Item* getItem(std::string);
	std::vector<Item*> getAllItems();
	void addItems(std::vector<Item*>);
	virtual std::string print();
};

