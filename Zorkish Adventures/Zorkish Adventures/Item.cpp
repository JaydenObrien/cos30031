#include "Item.h"

Item::Item(std::string id, std::string name, std::string desc): gameObject(id,name,desc)
{
}

Item::~Item()
{
}


void Item::addAttribute(Attribute* a)
{
	components.push_back(a);
}

std::string Item::printAttributes()
{
	std::string output = "";
	for (const auto& a : components)
	{
		output.append(a->print() + "\n");
	}
	if (output == "")
		return output;

	output.pop_back();
	return output;
}

std::string Item::printItem()
{
	std::string output = "";
	output.append("a " + this->getName() +": "+ this->getDescription() + "\n");
	
	for (const auto& a : components)
	{
		if (dynamic_cast<Inventory*>(a))
			continue;
		output.append(a->print() + "\n");
	}
	if (output == "")
		return output;

	output.pop_back();

	return output;
}

