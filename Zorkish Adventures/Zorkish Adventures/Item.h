#pragma once
#include "gameObject.h"
#include <map>
#include <vector>
#include "Inventory.h"

//game entity currently not working so i shove it in here
class Attribute;
class Item: public gameObject
{
private:
	std::vector<Attribute*> components;
public:
	Item(std::string id, std::string name, std::string desc);
	~Item();

	std::string printItem();

	template <typename T>
	T* getAttribute()
	{
		for (auto a : components)
		{
			if (dynamic_cast<T*>(a))
				return dynamic_cast<T*>(a);
		}
		return nullptr;
	}

	void addAttribute(Attribute*);
	
	template <typename T>
	bool hasAttribute()
	{
		for (auto a : components)
		{
			if (dynamic_cast<T*>(a))
				return true;
		}
		return false;
	}
	
	std::string printAttributes();
};
