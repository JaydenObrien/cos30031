#include "Location.h"



Location::Location(string id, string description, string name): gameObject(id, name, description)
{

}


Location::~Location()
{
}

void Location::addRoute(Location * l)
{
	routes.insert_or_assign(l->getName(), l);
}

//TODO: make this return a string
string Location::printRoutes()
{
	string temp = "";
	for (const auto& r : routes) 
	{
		temp.append(r.first + "\n");
	}
	temp.pop_back();
	return temp;
}

Location* Location::getRoute(string name)
{
	if (!routes.count(name))
		return nullptr;

	return routes[name];
}

Inventory* Location::getInventory()
{
	return &inv;
}
