#pragma once
#include <iostream>
#include <unordered_map>
#include <string>
#include <algorithm>
#include "gameObject.h"
#include "Inventory.h"

using namespace std;
class Location: public gameObject
{
private:
	unordered_map<string, Location*> routes;
	Inventory inv;
public:
	Location(string id, string description, string name);
	~Location();

	void addRoute(Location *l);
	string printRoutes();
	Location* getRoute(string name);
	Inventory* getInventory();
};

