#include "CommandInterface.h"



//TODO: make this prettier
void LookCommand::command(player& p, const std::vector<std::string>& tokens)
{
	//prints the items and routes in a locaiton
	if (tokens.size() == 1)
	{
		cout << "You are currently at " << p.getLocation()->getName() << endl;
		cout << "In the distance you see:" << endl;
		cout << p.getLocation()->printRoutes() << endl;
		cout << "On the ground you can see:" << endl;
		cout << p.getLocation()->getInventory()->print() << endl;
	}
	else if (tokens[1] == "at" && tokens.size() == 3)
	{
		if (tokens[2] == p.getName() || tokens[2] == "me")
		{
			cout << p.getDescription() << endl;
		}
		else if (tokens[2] == p.getLocation()->getName()) 
		{
			cout << p.getLocation()->getDescription() << endl;
		}
		else if (p.getInventory().getItem(tokens[2]) != nullptr)
		{
			cout << p.getInventory().getItem(tokens[2])->printItem() << endl;
		}
		else if (p.getLocation()->getInventory()->getItem(tokens[2]) != nullptr)
		{
			cout << p.getLocation()->getInventory()->getItem(tokens[2])->printItem() << endl;
		}
		else 
		{
			cout << "That thing your trying to look at doesn't exist" << endl;
		}
	}
	else if(tokens[1] == "in" && tokens.size() == 3)
	{
		if (tokens[2] == "me" || tokens[2] == p.getName())
		{
			cout << "You find the courage to go on" << endl;
		}
		else if (tokens[2] == "inventory" || tokens[2] == "bag")
		{
			cout << p.getInventory().print() << endl;
		}
		else if (p.getLocation()->getInventory()->getItem(tokens[2]) != nullptr)
		{
			if (p.getLocation()->getInventory()->getItem(tokens[2])->getAttribute<Inventory>() != nullptr)
			{
				if (p.getLocation()->getInventory()->getItem(tokens[2])->getAttribute<locked>()->isLocked())
					cout << "Can't look in that it is locked" << endl;
				else
					cout << p.getLocation()->getInventory()->getItem(tokens[2])->getAttribute<Inventory>()->print() << endl;
			}
			else
			{
				cout << "Cant look in that" << endl;
			}
			
		}
		else 
		{
			cout << "The thing that you are trying to look in doesn't exist" << endl;
		}
	}
	else if(tokens[1] == "at" && tokens[3] == "in" && tokens.size() == 5)
	{
		if (p.getLocation()->getInventory()->getItem(tokens[4]) != nullptr)
		{
			if (p.getLocation()->getInventory()->getItem(tokens[4])->getAttribute<Inventory>() != nullptr)
			{
				if (p.getLocation()->getInventory()->getItem(tokens[4])->getAttribute<Inventory>()->getItem(tokens[2]) != nullptr)
				{
					cout << p.getLocation()->getInventory()->getItem(tokens[4])->getAttribute<Inventory>()->getItem(tokens[2])->printItem() << endl;
				}
				else
				{
					cout << "The item your trying to look at doesnt exist" << endl;
				}
			}
			else 
			{
				cout << "You can't look in this item" << endl;
			}
		}
		else
		{
			cout << "The thing that you are trying to look in doesn't exist" << endl;
		}
	}
}