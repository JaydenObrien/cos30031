#include "CommandInterface.h"

void OpenCommand::command(player& p, const std::vector<std::string>& tokens)
{
	if (tokens.size() == 2)
	{
		if (p.getLocation()->getInventory()->getItem(tokens[1]))
		{
			if (p.getLocation()->getInventory()->getItem(tokens[1])->hasAttribute<locked>())
			{
				if (!p.getLocation()->getInventory()->getItem(tokens[1])->getAttribute<locked>()->unlock())
				{
					cout << "Need an item to unlocked that" << endl;
				}
				else
				{
					cout << "Unlocking " + tokens[1] << endl;
				}
			}
		}
	}
	else if (tokens.size() == 4 && tokens[2] == "with")
	{
		if (p.getLocation()->getInventory()->getItem(tokens[1]))
		{
			if (p.getLocation()->getInventory()->getItem(tokens[1])->hasAttribute<locked>())
			{
				if (p.getInventory().getItem(tokens[3]))
				{
					if (!p.getLocation()->getInventory()->getItem(tokens[1])->getAttribute<locked>()->unlock(p.getInventory().getItem(tokens[3])->getID()))
					{
						cout << "You need to use a different item to unlock that" << endl;
					}
					else
					{
						cout << "Unlocking " + tokens[1] + " with " + tokens[3] << endl;
					}
				}
				}
				else
				{
					cout << "You dont have that item in your inventory" << endl;
				}
			}
		}
		else
		{
			cout << "The item that you are trying to unlock doesn't exist" << endl;
		}
	}
