#include "CommandInterface.h"

void PutCommand::command(player& p, const std::vector<std::string>& tokens)
{
	if (tokens.size() == 3 && tokens[2] == "down") //put down
	{
		if (p.getInventory().getItem(tokens[1]))
		{
			p.getLocation()->getInventory()->addItem(p.getInventory().getItem(tokens[1]));
			p.getInventory().removeItem(tokens[1]);
			cout << "Putting " + tokens[1] + " on the ground" << endl;
		}
		else 
		{
			cout << "You don't have this item in your inventory" << endl;
		}
	}
	else if (tokens.size() == 4 && tokens[2] == "in") //put in
	{
		if (p.getInventory().getItem(tokens[1]))
		{
			if (p.getLocation()->getInventory()->getItem(tokens[3]))
			{
				if (p.getLocation()->getInventory()->getItem(tokens[3])->hasAttribute<Inventory>())
				{
					if (!p.getLocation()->getInventory()->getItem(tokens[3])->getAttribute<locked>()->isLocked())
					{
						p.getLocation()->getInventory()->getItem(tokens[3])->getAttribute<Inventory>()->addItem(p.getInventory().getItem(tokens[1]));
						p.getInventory().removeItem(tokens[1]);
						cout << "Putting " + tokens[1] + " in " + tokens[3] << endl;
					}
					else
					{
						cout << "It is locked" << endl;
					}
				}
				else
				{
					cout << "Can't put anthing in there" << endl;
				}
			}
			else
			{
				cout << "The item that you are trying to put into doesn't exist" << endl;
			}
		}
		else
		{
			cout << "You don't have this item in your inventory" << endl;
		}
	}
}