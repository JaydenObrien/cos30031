#pragma once
#include <iostream>
#include <string>
#include <experimental\filesystem>
#include <conio.h>
#include "StateMachine.h"
#include "gameWorld.h"
#include "pasta.h"
#include "CommandInterface.h"
#include "CommandProcessor.h"

using namespace std; 

class StateMachine;
class gameWorld;
class State
{
public:
	virtual ~State();
	virtual void start(StateMachine* sm) = 0;
	virtual void update(StateMachine* sm) = 0;
};

class mainMenuState : public State
{
public:
	virtual void start(StateMachine* sm);
	virtual void update(StateMachine* sm);
};

class aboutState : public State
{
public:
	virtual void start(StateMachine* sm);
	virtual void update(StateMachine* sm);
};

class hallOfFameState :public State
{
public:
	virtual void start(StateMachine* sm);
	virtual void update(StateMachine* sm);
};

class helpState : public State
{
public:
	virtual void start(StateMachine* sm);
	virtual void update(StateMachine* sm);
};

class newHighScoreState :public State
{
public:
	virtual void start(StateMachine* sm);
	virtual void update(StateMachine* sm);
};

class selectAdventureState :public State
{
public:
	virtual void start(StateMachine* sm);
	virtual void update(StateMachine* sm);
};

class gamePlayState :public State
{
private: 
	gameWorld *world;
	CommandProcessor cp;
	std::string filepath;
public:
	gamePlayState(string filePath);
	~gamePlayState();

	virtual void start(StateMachine* sm);
	virtual void update(StateMachine* sm);
};




