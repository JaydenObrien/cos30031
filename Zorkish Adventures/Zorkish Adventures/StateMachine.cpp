#include "StateMachine.h"

StateMachine::StateMachine() {}

StateMachine::~StateMachine() 
{
	delete current;
}

void StateMachine::setState(State *state)
{
	delete current;
	current = state;
}

State* StateMachine::getCurrentState()
{
	return current;
}
