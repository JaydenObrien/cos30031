#pragma once
#include "State.h"

class State;
class StateMachine
{
private:
	State* current;
public:
	StateMachine();
	~StateMachine();
	
	void setState(State *state);
	State* getCurrentState();

};

