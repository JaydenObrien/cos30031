#include "CommandInterface.h"

void TakeCommand::command(player& p, const std::vector<std::string>& tokens)
{
	if (tokens.size() == 2)
	{
		if (p.getLocation()->getInventory()->getItem(tokens[1]) != nullptr)
		{
			if (!p.getLocation()->getInventory()->getItem(tokens[1])->hasAttribute<Immovable>())
			{
				p.getInventory().addItem(p.getLocation()->getInventory()->getItem(tokens[1]));
				p.getLocation()->getInventory()->removeItem(tokens[1]);
				cout << "Putting " + tokens[1] + " into your inventory" << endl;
			}
			else
			{
				cout << "This item cant be moved" << endl;
			}
			
		}
		else
		{
			cout << "This item doesn't exist" << endl;
		}
	}
	else if (tokens.size() == 4 && tokens[2] == "from")
	{
		if (p.getLocation()->getInventory()->getItem(tokens[3]) != nullptr)
		{
			if (p.getLocation()->getInventory()->getItem(tokens[3])->hasAttribute<Inventory>())
			{
				if (!p.getLocation()->getInventory()->getItem(tokens[3])->getAttribute<locked>()->isLocked())
				{
					if (p.getLocation()->getInventory()->getItem(tokens[3])->getAttribute<Inventory>()->getItem(tokens[1]) != nullptr)
					{
						p.getInventory().addItem(p.getLocation()->getInventory()->getItem(tokens[3])->getAttribute<Inventory>()->getItem(tokens[1]));
						p.getLocation()->getInventory()->getItem(tokens[3])->getAttribute<Inventory>()->removeItem(tokens[1]);
						cout << "Putting " + tokens[1] + " into your inventory" << endl;
					}
					else
					{
						cout << "The item that you are trying to take doesn't exist" << endl;
					}
				}
				else
				{
					cout << "It is locked" << endl;
				}
				
			}
			else
			{
				cout << "You can't take anything from there" << endl;
			}
		}
		else
		{
			cout << "The item that you are trying to take from doesn't exist" << endl;
		}
	}


}