#include "gameObject.h"



gameObject::gameObject(std::string id, std::string name, std::string desc)
{
	this->name = name;
	this->id = id;
	description = desc;
}


gameObject::~gameObject()
{
}

std::string gameObject::getName()
{
	return name;
}

std::string gameObject::getDescription()
{
	return description;
}

std::string gameObject::getID()
{
	return id;
}
