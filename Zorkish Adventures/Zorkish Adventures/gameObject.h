#pragma once
#include <string>

class gameObject
{
private: 
	std::string name;
	std::string description;
	std::string id;
public:
	gameObject(std::string id, std::string name, std::string desc);
	~gameObject();

	std::string getName();
	std::string getDescription();
	std::string getID();
};

