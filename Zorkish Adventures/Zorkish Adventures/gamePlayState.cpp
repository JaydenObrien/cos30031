#include "State.h"
#include "CommandProcessor.h"

using namespace std;

gamePlayState::gamePlayState(string filePath)
{
	this->filepath = filePath;
}

gamePlayState::~gamePlayState() 
{
	delete world;
}

void gamePlayState::start(StateMachine* sm)
{
	world = new gameWorld();
	try
	{
		world->loadGameWorld(filepath);
	}
	catch (exception e)
	{
		cout << "Failed to load game file " + filepath << endl;
		delete world;
		sm->setState(new selectAdventureState());
	}
}

void gamePlayState::update(StateMachine* sm)
{
	while (sm->getCurrentState() == this)
	{
		string input;
		vector<string> commands;
		cout << ":> ";
		getline(cin >> ws, input);
		pasta::pasta(input, ' ', commands);

		if (input == "hiscore")
		{
			sm->setState(new newHighScoreState());
		}
		else if (input == "quit")
		{
			sm->setState(new mainMenuState());
		}
		else if (cp.hasCommand(commands))
		{
			cp.command(*world->getPlayer(), commands);
		}
		else
		{
			cout << "Invalid Input" << endl;
		}
		commands.clear();
	}
}