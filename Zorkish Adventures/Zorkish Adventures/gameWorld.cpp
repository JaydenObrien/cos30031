#include "gameWorld.h"

#define log(x) std::cout << x << std::endl;

gameWorld::gameWorld()
{

}


gameWorld::~gameWorld()
{
	delete player1;
}

//This scares me to look at
void gameWorld::loadGameWorld(string filePath)
{
	try
	{
		fstream file;
		file.open(filePath, ios::in, ios::binary);
		if (file.is_open())
		{
			string line;
			vector<string> parString;
			vector<string> routeIDs;
			map<string, vector<string>> locationItems;

			vector<string> attruibutes;
			map<string, string> attruibuteValues;
			//map<string, Item*> items;
			map<string, map<string, string>> itemAttrbutesCombo;
			map<string, vector<string>> itemInventory;

			string playerStartingPos;
			map<string, vector<string>> placeHolderName;
			while (getline(file, line))
			{
				pasta::pasta(line, ';', parString);
				if (parString.at(0) == "loc")
				{
					vector<string> temp;
					mapData.insert_or_assign(parString.at(1), new Location(parString.at(1), parString.at(3), parString.at(2)));
					pasta::pasta(parString.at(4), ',', routeIDs);
					placeHolderName.insert_or_assign(parString.at(1), routeIDs);
					if (parString[5] != "-")
					{
						pasta::pasta(parString[5], ',', temp);
						locationItems.insert_or_assign(parString[1], temp);
					}

				}
				else if (parString.at(0) == "ply")
				{
					player1 = new player(parString.at(1), parString.at(1), "-");
					playerStartingPos = parString.at(2);
				}
				else if (parString[0] == "itm")
				{
					itemData.insert_or_assign(parString[1], new Item(parString[1], parString[2], parString[3]));
					if (parString[4] != "-")
					{
						pasta::pasta(parString[4], ',', attruibutes);
						for (const auto& a : attruibutes)
						{
							vector<string> temp;
							pasta::pasta(a, '=', temp);
							attruibuteValues.insert_or_assign(temp[0], temp[1]);
							temp.clear();
						}
						itemAttrbutesCombo.insert_or_assign(parString[1], attruibuteValues);
					}

				}
				attruibuteValues.clear();
				attruibutes.clear();
				routeIDs.clear();
				parString.clear();
			}

			//assigns item attrubutes
			for (auto i : itemAttrbutesCombo)
			{
				for (auto k : i.second)
				{
					if (k.first == "hp")
						itemData[i.first]->addAttribute(new HealthPoints(stoi(k.second)));
					if (k.first == "inv")
					{
						vector<string> temp;
						itemData[i.first]->addAttribute(new Inventory());
						pasta::pasta(k.second, ',', temp);
						itemInventory.insert_or_assign(i.first, temp);
					}
					if (k.first == "dmg")
					{
						itemData[i.first]->addAttribute(new damage(stoi(k.second)));
					}
					if (k.first == "lock")
					{
						if (k.second == "true")
						{
							itemData[i.first]->addAttribute(new locked(true, ""));
						}
						else
						{
							itemData[i.first]->addAttribute(new locked(true, k.second));
						}
					}
					if (k.first == "immovable")
					{
						itemData[i.first]->addAttribute(new Immovable());
					}
				}
			}

			//add items to items with the inv attribute
			for (auto i : itemInventory)
			{
				for (auto k : i.second)
				{
					itemData[i.first]->getAttribute<Inventory>()->addItem(itemData[k]);

				}

			}

			//links routes
			for (auto m : placeHolderName)
			{
				for (auto k : m.second)
				{
					mapData[m.first]->addRoute(mapData[k]);
				}
			}

			//add items to lcoaitons
			for (auto pls : locationItems)
			{
				for (auto v : pls.second)
				{
					mapData[pls.first]->getInventory()->addItem(itemData[v]);
				}
			}

			//adds playerStarting posision
			player1->setLocation(mapData[playerStartingPos]);
		}
		file.close();
	}
	catch (...)
	{
		throw std::exception("Nooo");
	}
}

player* gameWorld::getPlayer()
{
	return player1;
}
