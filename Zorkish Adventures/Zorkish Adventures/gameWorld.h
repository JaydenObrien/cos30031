#pragma once
#include <string>
#include <vector>
#include <fstream>
#include <map>
#include <sstream>
#include "Location.h"
#include "player.h"
#include "pasta.h"
#include "Attribute.h"
#include "Inventory.h"

using namespace std;
class player;
class Attribute;
class Location;
class Inventory;
class damage;
class HealthPoints;
class Item;
class gameWorld
{
private:
	player *player1;
	map<string,Location*> mapData;
	map<string, Item*> itemData;
public:
	gameWorld();
	~gameWorld();

	void loadGameWorld(string filePath);
	
	void addPlayer(player *p) {
		player1 = p;
	}

	void printMapData() {
		for (auto& a : mapData) {
			cout << a.first << a.second->getName() << endl;
		}
	}

	void printItemData()
	{
		for (auto& a : itemData) {
			cout << a.first << a.second->getName() << endl;
		}
	}

	player* getPlayer();
};

