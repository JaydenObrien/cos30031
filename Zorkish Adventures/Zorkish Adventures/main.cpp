#include <iostream>
#include <conio.h>
#include "StateMachine.h"
#include "Location.h"
#include "player.h"
#include "gameWorld.h"
#include "Enemy.h"

#define log(x) std::cout << x << std::endl;


int main()
{	
	StateMachine main;
	main.setState(new mainMenuState());
	
	for (;;)
	{
		main.getCurrentState()->start(&main);
		main.getCurrentState()->update(&main);
		if (!main.getCurrentState())
			break;
	}
	
	/*Enemy e1;
	e1.init();
	cout << e1.name << endl;
	cout << e1.desc << endl;
	cout << e1.PosX << " , " << e1.PosY << endl;
	cout << e1.HP << endl;*/



	_getch();
	return 0;
}
