#include "State.h"


void mainMenuState::start(StateMachine* sm) 
{
	int input = 0;
		cout << "Welcome to the Zorkish " << endl;
		cout << "1. Play" << endl;
		cout << "2. Hall of Fame" << endl;
		cout << "3. Help" << endl;
		cout << "4. About" << endl;
		cout << "5. Quit" << endl;
		cout << "Select 1-5:> "; //presume rigt input

		cin >> input;
		switch (input)
		{
		case 1:
			sm->setState(new selectAdventureState());
			break;
		case 2:
			sm->setState(new hallOfFameState());
			break;
		case 3:
			sm->setState(new helpState());
			break;
		case 4:
			sm->setState(new aboutState());
			break;
		case 5:
			sm->setState(nullptr);
			break;
		default:
			cout << "Invalid Command" << endl;
			break;
		}
}

void mainMenuState::update(StateMachine* sm) {}
