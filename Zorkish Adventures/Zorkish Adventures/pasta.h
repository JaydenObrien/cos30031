#pragma once
#include <vector>
#include <string>
#include <sstream>
#include <iostream>

namespace pasta 
{
	std::vector<std::string> &pasta(const std::string &s, char delim, std::vector<std::string> &elems);
}