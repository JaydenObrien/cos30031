#pragma once
#include "Location.h"
#include "Inventory.h"
#include <string>

using namespace std;

class Inventory;
class player: public gameObject
{
private:
	Inventory inv;
	Location *location;

public:
	player(string id, string name, string desc);
	~player();

	Location* getLocation();
	void setLocation(Location *location);
	Inventory& getInventory();
};

