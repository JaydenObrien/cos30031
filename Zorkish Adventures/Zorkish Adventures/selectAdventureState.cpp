#include "State.h"

#define gameWorldFolder "C:/Users/Jayden/Desktop/cos30031/Zorkish Adventures/gameWorlds"
#define characterFilePathOfSet sizeof(gameWorldFolder)
using namespace std;


//TODO: change the experimental filesystem when VS gets updated
void selectAdventureState::start(StateMachine* sm)
{
	

}

void selectAdventureState::update(StateMachine* sm)
{
	int input = 0;
	int i = 1;
	map<int, string> filePaths;

	cout << "Choose you adventure:" << endl;

	for (auto& p : experimental::filesystem::directory_iterator(gameWorldFolder))
	{
		ostringstream temp;
		temp << p;
		string output = temp.str();
		filePaths.insert_or_assign(i, output);
		output.erase(0, characterFilePathOfSet);
		output.erase(output.end() - 4, output.end());
		cout << i << ". " << output << endl;
		i++;
	}

	cout << "Select :> ";
	cin >> input;
	//TODO: validate input here
	sm->setState(new gamePlayState(filePaths[input]));
}
